"""
The module represents subsidiary technical code used in the 'models.py'
(App 'teachers').
"""

import random

from faker import Faker


def make_profile():
    """
    This method makes fake profile data on a lecturer.

    :return first_name, last_name, age,
            date_of_birth, email, mobile_number,
            education, exp_years, groups: (str/int) fake profile parameters

    Local variables:
    fake: (Faker) an instance of the class 'Faker' from the module 'faker'
                  used to make some fake profile data
    mail_first, mail_last, OPERATORS, specializations:
    (str/dict) additional data to generate certain profile parameters of
    """
    fake = Faker()
    first_name = fake.first_name()
    last_name = fake.last_name()
    age = random.randint(21, 60)
    date_of_birth = fake.date_of_birth(minimum_age=age, maximum_age=age)
    mail_first = first_name.lower()
    mail_last = last_name.lower()
    email = random.choice([f"{mail_first}_{mail_last}",
                           f"{mail_first}-{mail_last}",
                           f"{mail_first}{mail_last}",
                           f"{mail_first}{random.randint(111, 10000)}{mail_last}",
                           f'{mail_first}{random.randint(111, 10000)}',
                           f'{mail_first}',
                           f'{mail_first[0]}_{mail_last}',
                           f'{mail_last}_{mail_first}',
                           f'{mail_last}-{mail_first}'
                           ]) + '@' + fake.free_email_domain()
    OPERATORS = [39, 50, 63, 66, 67, 68, 93, 95, 96, 97, 98, 99]  # noqa
    mobile_number = f"+38 (0{random.choice(OPERATORS)}) " \
                    f"{random.randint(100, 999)}-{random.randint(10, 99)}-{random.randint(10, 99)}"
    specializations = {"Java Developer": ['Introduction Java', 'Java Elementary', 'Java Enterprise'],
                       "Web Developer": ['Front-End Basic', 'Front-End Pro', 'React'],
                       "PHP Developer": ['PHP', 'Basic PHP'],
                       "Python Developer": ['Introduction Python', 'Advanced Python'],
                       "DevOps Engineer": ['DevOps'],
                       "Data Scientist": ['Machine Learning'],
                       "QA Specialist": ['QA', 'QA Automation'],
                       "IT Manager": ['Project Management', 'HR IT', 'Business Analysis'],
                       "IT Designer": ['Design', 'UI/UX Design']}
    education = random.choice(list(specializations))
    exp_years = age - random.randint(18, age)
    # groups = random.choices(specializations[education],
    #                         k=random.randint(1, 3))
    # return first_name, last_name, age, date_of_birth, email, mobile_number, education, exp_years, groups
    return first_name, last_name, age, date_of_birth, email, mobile_number, education, exp_years


def generate_teacher():
    """
    The method unpacks fake profile data on a teacher into a dictionary.

    :return teacher: (dict) generated profile with fake data

    Local variables:
    data: (tuple) fake profile data on a lecturer
    """
    data = make_profile()
    teacher = {'first_name': data[0], 'last_name': data[1], 'age': data[2], 'date_of_birth': data[3], 'email': data[4],
               'mobile_number': data[5], 'education': data[6], 'exp_years': data[7]}
    # 'mobile_number': data[5], 'education': data[6], 'exp_years': data[7], 'groups': data[8]}
    return teacher


def modify_data(teacher):
    """
    The method alters a teacher's profile in the table
    by rewriting instance attributes with unpacked
    newly generated fake profile data.

    :return teacher: (Teacher) an instance of the model Teacher
                               that contains updated profile data
    Local varaibles:
    teacher.first_name, teacher.last_name, teacher.age,
    teacher.date_of_birth, teacher.email, teacher.mobile_number,
    teacher.education, teacher.exp_years, teacher.groups:
    (str/int) instance attributes with renewed fake profile parameters
    """
    teacher.first_name, teacher.last_name, teacher.age, \
        teacher.date_of_birth, teacher.email, teacher.mobile_number, \
        teacher.education, teacher.exp_years = make_profile()
    # teacher.education, teacher.exp_years, teacher.groups = make_profile()
    return teacher
