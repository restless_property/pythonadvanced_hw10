from django.core.management.base import BaseCommand, CommandError # noqa

from teachers.models import Teacher


class Command(BaseCommand):
    args = '<the id of the teacher whose data is to be replaced with relevant random values>'
    help = 'Updates the data on a specified teacher with random fake information'  # noqa: A003

    def add_arguments(self, parser):
        parser.add_argument('pk', type=int)

    def handle(self, *args, **options):
        return Teacher.update_teacher(pk=options['pk'])
