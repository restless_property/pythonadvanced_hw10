from django.core.management.base import BaseCommand, CommandError # noqa

from teachers.models import Teacher


class Command(BaseCommand):
    args = '<the number of teachers to generate>'
    help = 'Generates the specified number of lecturers'  # noqa: A003

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        return Teacher.generate_teachers(count=options['count'])
