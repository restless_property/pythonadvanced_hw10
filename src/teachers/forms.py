"""
The module reciprocally processes HTML code and 'Python'-related one.
(App 'teachers')
"""
import datetime

import django_filters
from django.core.exceptions import ValidationError  # noqa
from django.forms import DateInput, ModelForm

from teachers.models import Teacher


class DateInputForm(DateInput):
    """
    This class is used to apply a custom HTML input model
    for attributes with type 'models.DateField'.
    """
    input_type = 'date'


class TeacherFilter(django_filters.FilterSet):

    class Meta:
        model = Teacher
        fields = {
            'first_name': ['exact', 'startswith', 'icontains'],
            'last_name': ['exact', 'startswith', 'icontains'],
            'age': ["exact", "gt", "gte", "lt", "lte"],
            'date_of_birth': ["exact"],
            'email': ["exact", "icontains"],
            'mobile_number': ["exact", "icontains"],
            'education': ["exact", "icontains"],
            'exp_years': ["exact", "gt", "gte", "lt", "lte"],
            'group': ["exact"]
        }
        widgets = {'date_of_birth': DateInputForm()}


class TeacherBaseForm(ModelForm):
    """
    A form that converts "python"-related data types of instance attributes
    into relevant HTML representation.
    """

    class Meta:
        model = Teacher
        # fields = ['first_name', "last_name", "age",
        #           "date_of_birth", "email", "mobile_number",
        #           "education", "exp_years"]
        fields = '__all__'
        widgets = {'date_of_birth': DateInputForm()}

    def as_p_br(self):
        """
        Returns the form rendered as HTML <p>s (with extra <break>'s).
        """
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s <br/> %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean_email(self):
        """
        Validates that the input email is unique.
        """
        email = self.cleaned_data['email']
        email_not_unique = Teacher.objects.filter(email=email).exclude(id=self.instance.id).exists()
        if email_not_unique:
            raise ValidationError("This email is not unique.")
        return email

    def clean(self):
        """
        Validates that the age and the date of birth of a teacher
        input through an HTML form mutually correspond.
        """
        age = self.cleaned_data['age']
        dob = self.cleaned_data['date_of_birth']
        validation = super().clean()
        if age and dob:
            today = datetime.date.today()
            verified_age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
            if age != verified_age:
                raise ValidationError('The input data in fields "Age" and "Date of Birth" do not match.')
        return validation


class TeacherCreateForm(TeacherBaseForm):
    model = Teacher
    pass


class TeacherUpdateForm(TeacherBaseForm):

    class Meta(TeacherBaseForm.Meta):
        model = Teacher
