"""
The module contains auxiliary functions used by the 'views.py' module
(App 'teachers').
"""


def format_table(records):
    """
    Formats the content of a 'Python' collection.

    :param records: (collection) a 'Python' collection
    :return response: (str) a formatted content of the collection

    Local variables:
    line: (obj) one item of the collection
    """
    response = ""
    for line in records:
        link = '<a href="{% url \'teachers_edit\' line.id %}">[UPDATE]</a>'
        response += "<br>" + link + str(line)
    return response if response else '&ltNo results for your query&gt'


def repack_query_set(qs):
    """
    The function reforms a QuerySet into a list
    to be used in HTML template rendering.

    :param qs: (QuerySet) a QS with parsed data
    :return data: (list) results of an SQL query
                         repacked into a list

    Local variables:
    qs_values: (QuerySet) values of the QS fields [dict]
    dict_list: (list) QS values [dict] repacked into a list
    voc: (dict) one dictionary-item inside the list of QS values
    """
    qs_values = qs.values()
    dict_list = list(qs_values)
    data = [voc.values() for voc in dict_list]
    return data
