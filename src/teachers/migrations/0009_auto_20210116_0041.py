# Generated by Django 3.1.4 on 2021-01-15 22:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0008_remove_group_lecturer'),
        ('teachers', '0008_auto_20210116_0039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='teachers', to='groups.group', verbose_name='Group'),
        ),
    ]
