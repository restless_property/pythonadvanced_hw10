# Generated by Django 3.1.4 on 2021-01-15 23:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0009_auto_20210116_0041'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teacher',
            name='group',
        ),
    ]
