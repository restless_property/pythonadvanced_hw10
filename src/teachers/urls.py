"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from debug_toolbar import settings  # noqa

from django.urls import path

# from teachers.views import create_teachers, delete_teacher, get_teachers_with_filter, update_teacher
from teachers.views import TeacherCreateView, TeacherDeleteView, TeacherFilterView, TeacherUpdateView
from teachers.views import render_teachers_as_list, render_teachers_as_table

app_name = "teachers"

urlpatterns = [
    path('', render_teachers_as_table, name='table'),
    # path('filter', get_teachers_with_filter, name='filter'),
    path('list', render_teachers_as_list, name='list'),
    # path('create', create_teachers, name='new'),
    # path('update/<int:pk>', update_teacher, name='edit'),
    # path('delete/<int:pk>', delete_teacher, name='delete'),
    path('filter', TeacherFilterView.as_view(), name='filter'),
    path('create', TeacherCreateView.as_view(), name='new'),
    path('update/<int:pk>', TeacherUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>', TeacherDeleteView.as_view(), name='delete'),

]
