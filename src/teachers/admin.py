from django.contrib import admin  # noqa

# Register your models here.
from teachers.models import Teacher


class TeacherAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'first_name', 'last_name', 'age']
    filter_fields = ['id', 'first_name', 'last_name', 'age']  # <&>-filter
    search_fields = ['id', 'first_name', 'last_name', 'age']  # <|>-filter
    list_select_related = ['group']


admin.site.register(Teacher, TeacherAdmin)
