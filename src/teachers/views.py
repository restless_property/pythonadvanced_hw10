"""
Django module with view-functions (App 'teachers').
"""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect  # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teachers.forms import TeacherCreateForm, TeacherFilter, TeacherUpdateForm
from teachers.models import Teacher
from teachers.tools import format_table, repack_query_set # noqa

# Create your views here.


def get_teachers(request):
    """
    This method outputs records from the DB table 'teachers_teacher'
    according to an SQL request depending on filter parameters.

    :param request: (str) a request received by the server from a client
    :return teachers: (QS) fetched from the table records on teachers

    Local variables:
    teachers: (QuerySet) records of the table 'teachers_teacher'
                         according to filter parameters
    params: (list) admissible SQL filtering parameters
    param_name: (str) one filtering parameter from the list
    param_value: (str) a value input as a certain parameter (param_name)
    param_merged: (str) one or several values of the parameter input by a client
    param_elems: (list) parameter values stripped of redundant space symbols
    param_elem: (str) one separate value from the list of parameter values
    or_filter: (Q) a filtering utility which
                   applies the logical operator "OR" to SQL requests
    """
    teachers = Teacher.objects.all()
    params = ['first_name',
              'first_name__startswith',
              'first_name__endswith',
              'last_name',
              'last_name__startswith',
              'last_name__endswith',
              'age',
              'age__gt',
              'age__gte',
              'age__lt',
              'age__lte',
              'date_of_birth',
              'date_of_birth__startswith',
              'date_of_birth__endswith',
              'e_mail',
              'e_mail__startswith',
              'e_mail__endswith',
              'education',
              'education__startswith',
              'education__endswith',
              'mobile_number',
              'exp_years',
              'exp_year__gt',
              'exp_years__gte',
              'exp_years__lt',
              'exp_years__lte',
              'groups',
              'groups__startswith',
              'groups__endswith'
              ]
    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_merged = param_value.split(',')
            param_elems = list(map(lambda x: x.strip(), param_merged))
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    if isinstance(request.GET.get('precise'), str) or \
                            (param_name == 'age' or param_name == 'exp_years'):
                        or_filter |= Q(**{param_name: param_elem})
                    else:
                        or_filter |= Q(**{f'{param_name}__contains': param_elem})
                teachers = teachers.filter(or_filter)
            else:
                teachers = teachers.filter(**{param_name: param_value})
    return teachers


def render_teachers_as_table(request):
    """
    This method renders data on teachers as an HTML table.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered HTML code

    Local variables:
    teachers: (QS) records of the table 'teachers_teacher'
                   according to filter parameters
    data: (list) results of an SQL query repacked into a list
    headers: (list) a list of the names of the table columns
    """
    teachers = get_teachers(request)
    data = repack_query_set(teachers)
    # try:
    #     headers = [f.verbose_name for f in teachers[0]._meta.fields]
    # except IndexError:
    #     headers = []
    return render(request, "teachers/teachers_get.html",
                  {"data": data})
    # {"headers": headers, "data": data})


def render_teachers_as_list(request):
    """
    This method renders data on teachers as HTML paragraphs.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered HTML code

    Local variables:
    teachers: (QS) records of the table 'teachers_teacher'
                   according to filter parameters
    """
    teachers = get_teachers(request)
    return render(request=request,
                  template_name='teachers/teachers_list.html',
                  context={'records': teachers})


def get_teachers_with_filter(request):
    """
    This method outputs records from the DB table 'teachers_teacher'
    according to an SQL request formed with a 'django_filters.FilterSet'.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered HTML code

    Local variables:
    teachers: (QS) all the records from the table 'teachers_teacher' of the DB.
    filt: ('django_filters.FilterSet') a class that renders preferred attributes of
                                       instances of the model "Teacher" into HTML code
    """
    teachers = Teacher.objects.all().select_related("group")
    # teachers = Teacher.objects.all()
    # headers = []
    # if teachers:
    #     headers = [f.verbose_name for f in teachers[0]._meta.fields]
    #     headers.extend(["🖉", "🗑"])
    filt = TeacherFilter(
        data=request.GET,
        queryset=teachers)
    return render(request=request,
                  template_name='teachers_filter/teachers_get_filter.html',
                  context={'filter': filt})
    # context={'filter': filt, "headers": headers})


def create_teachers(request):
    """
    This method generates a new teacher
    and adds him/her into the 'teachers_teacher' table of the DB.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    form: (user's Modelform) parameters to post and input into the DB
                             reciprocally rendered into HTML and 'Python' code
    html_template: (str) HTML code to be rendered into the web-page
    result: (str) formatted HTML code with the values of parameters
                  rendered as paragraphs/redirects to the table 'teachers_teacher'
    """
    if request.method == 'POST':
        form = TeacherCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:filter'))
    elif request.method == 'GET':
        form = TeacherCreateForm()
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    # return render(request, 'teachers/teachers_create.html', {"fields": form.as_p_br()})  # without 'FilterSet'
    return render(request, 'teachers/teachers_create.html', {"form": form})


def update_teacher(request, pk):
    """
    This method updates information on a teacher
    whose the table id is indicated as a parameter

    :param request: (str) a request received by the server from a client
    :param pk: (int) an id of a teacher in the table 'teachers_teacher'
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    teacher: (Teacher) a lecturer in the table with the specified id
    form: (user's Modelform) parameters to post and input into the DB
                             reciprocally rendered into HTML and 'Python' code
    """
    teacher = get_object_or_404(Teacher, id=pk)
    if request.method == 'POST':
        form = TeacherUpdateForm(data=request.POST, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:filter'))
    elif request.method == 'GET':
        form = TeacherUpdateForm(instance=teacher)
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request=request,
                  template_name='teachers/teachers_update.html',
                  # context={'fields': form.as_p_br(),  # without 'FilterSet'
                  context={'form': form,
                           'id': pk,
                           'teacher': teacher})


def delete_teacher(request, pk):
    """
    This method deletes a specified teacher
    from the relevant table of the DB.

    :param request: (str) a request received by the server from a client
    :param pk: (int) an id of a teacher in the table 'teachers_teacher'
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    teacher: (Teacher) a lecturer in the table with the specified id
    """
    teacher = get_object_or_404(Teacher, id=pk)
    if request.method == 'POST':
        teacher.delete()
        return HttpResponseRedirect(reverse('teachers:filter'))
    return render(request=request,
                  template_name='teachers/teachers_delete.html',
                  context={'teacher': teacher})


class TeacherFilterView(LoginRequiredMixin, ListView):
    """
    Searches and filters teachers.
    """
    model = Teacher
    template_name = 'teachers_filter/teachers_get_filter.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 10
    context_object_name = 'teachers'

    def get_filter(self):
        return TeacherFilter(
               data=self.request.GET,
               queryset=self.model.objects.all()
               )

    def get_queryset(self):
        return self.get_filter().qs.select_related("group")

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        # context['teachers'] = self.get_filter().qs.select_related("group")
        return context


class TeacherCreateView(LoginRequiredMixin, CreateView):
    """
    Creates a new teacher.
    """
    model = Teacher
    form_class = TeacherCreateForm
    success_url = reverse_lazy('teachers:filter')
    template_name = 'teachers/teachers_create.html'
    login_url = reverse_lazy('accounts:login')


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    """
    Updates a teacher.
    """

    model = Teacher
    form_class = TeacherUpdateForm
    success_url = reverse_lazy('teachers:filter')
    template_name = 'teachers/teachers_update.html'
    login_url = reverse_lazy('accounts:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.get_object().id
        return context


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    """
    Deletes a teacher.
    """

    model = Teacher
    success_url = reverse_lazy('teachers:filter')
    template_name = 'teachers/teachers_delete.html'
    login_url = reverse_lazy('accounts:login')
