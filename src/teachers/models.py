"""
The module is a realization for lecturers of groups in LMS.
(App 'teachers').
"""
import random

from core.models import Person
from core.validators import validate_age, validate_date_of_birth, validate_phone_number_v2

from django.db import models
from django.shortcuts import get_object_or_404

from faker import Faker

from teachers.model_tools import generate_teacher, modify_data


class Teacher(Person):
    """
    The class contains class attributes
    for the rows of the table 'teachers_teacher'
    and covers personal information on a lecturer of a group.

    Methods:
    generate_teachers: generates a range of fake lecturers' profiles
    update_teacher: updates an existing profile of a teacher with pointed id
                    with random fake data
    """

    age = models.PositiveIntegerField(null=True,
                                      blank=True,
                                      verbose_name="Age (y.o.)",
                                      validators=[
                                         validate_age,
                                      ])
    date_of_birth = models.DateField(null=True,
                                     verbose_name="Date of Birth",
                                     validators=[
                                         validate_date_of_birth
                                     ])
    # email = models.EmailField(null=False,
    #                           verbose_name="Email",
    #                           validators=[validate_email_for_prohibited_domain]
    #                           )
    mobile_number = models.CharField(max_length=21,
                                     null=True,
                                     blank=True,
                                     verbose_name="Mobile Number",
                                     validators=[
                                         validate_phone_number_v2
                                     ])
    education = models.TextField(null=True, blank=True, verbose_name="Education")
    exp_years = models.PositiveIntegerField(null=True, blank=True, verbose_name="Experience (y.)")
    # groups = models.TextField(null=True, verbose_name="Groups")
    # qualification = models.IntegerChoices  # TODO: fill in
    group = models.ForeignKey(
        to='groups.Group',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='teachers',
        verbose_name='Group'
    )

    def __str__(self):
        """
        A printable representation of one row
        inside the table 'teachers_teacher'
        as an instance of the class 'Teacher'.

        :return output: (str) profile data on a lecturer
        """
        return f'#ID: {self.id}: {self.get_full_name()}, {self.education}'

    def get_info(self):
        output = f'Name: {self.get_full_name()} - ' \
                 f'Age: {self.age} - ' \
                 f'DoB: {self.date_of_birth} - ' \
                 f'Email: {self.email} - ' \
                 f'Mobile number: {self.mobile_number} - ' \
                 f'Education: {self.education} - ' \
                 f'Experience: {self.exp_years} y'
        if self.group:
            output += f' - Group: {self.group.group_name}'
        return output

    @classmethod
    def generate_teachers(cls, count: int):
        """
        The method generates a specified number of lecturers
        and adds them to a DB as records.

        :param count: (int) a number of lecturers to generate

        Local variables:
        fake: (Faker) an instance of the 'Faker' class
        _: (int) unused sequential numbers of the counter
        teacher: (dict) fake profile of a teacher
        obj: (Teacher) an instance of the corresponding class
                       (one fake lecturer)
        """
        if count <= 0:
            raise ValueError("Cannot add a negative or zero number of records into the DB.")
        for _ in range(count):
            teacher = generate_teacher()
            obj = cls(**teacher)
            obj.save()
            # ----- #
            # obj = cls(
            #           first_name=teacher['first_name'],
            #           last_name=teacher['last_name'],
            #           age=teacher['age'],
            #           date_of_birth=teacher['date_of_birth'],
            #           email=teacher['email'],
            #           mobile_number=teacher['mobile_number'],
            #           education=teacher['education'],
            #           exp_years=teacher['exp_years'],
            #           # group=teacher['groups']
            #          )
            # obj.save()

    @classmethod
    def update_teacher(cls, pk: int):
        """
        The method alters information of one row of the table 'teachers_teacher'
        on a specified lecturer and saves the correspondent changes in the DB.

        :param pk: (int) the id of a teacher whose profile is to be modified

        Local variables:
        teacher_qs: (QS) fetched record of a teacher
                        whose id in the table was specified as a filtering parameter
        teacher_current: (Teacher) an instance of the model (one row in the table)
                                   with relevant profile data
        teacher_updated: (Teacher) an instance of the model (one row in the table)
                                   with updated profile data
        """
        if pk < 1:
            raise ValueError("The parameter 'pk' cannot be smaller than '1'.")
        teacher_qs = get_object_or_404(Teacher, id=pk)
        teacher_current = teacher_qs[0]
        teacher_updated = modify_data(teacher_current)
        teacher_updated.save()

    @classmethod
    def fill_emails(cls):
        """
        The method fills in the field 'email'
        for all the existing instances of the model 'Teacher'.
        """
        fake = Faker()
        teachers = cls.objects.all()
        for teacher in teachers:
            mail_first = teacher.first_name.lower()
            mail_last = teacher.last_name.lower()
            teacher.email = random.choice([f"{mail_first}_{mail_last}",
                                           f"{mail_first}-{mail_last}",
                                           f"{mail_first}{mail_last}",
                                           f"{mail_first}{random.randint(111, 10000)}{mail_last}",
                                           f'{mail_first}{random.randint(111, 10000)}',
                                           f'{mail_first}',
                                           f'{mail_first[0]}_{mail_last}',
                                           f'{mail_last}_{mail_first}',
                                           f'{mail_last}-{mail_first}'
                                           ]) + '@' + fake.free_email_domain()
            teacher.save()
