"""
THe module contains view-functions for the App. "accounts".
"""

from accounts.forms import AccountAndProfileUpdateForm, AccountPasswordUpdateForm, AccountRegistrationForm, \
    AccountUpdateForm

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView
from django.views.generic.edit import DeleteView, ProcessFormView


# Create your views here.

User = get_user_model()


class AccountRegistrationView(CreateView):
    """
    The class operates the process of the registration of a new user.
    """
    model = User
    template_name = 'accounts/registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful registration.
        """
        result = super().form_valid(form)
        messages.success(self.request, 'A new user has been successfully registered.')
        return result


class AccountLoginView(LoginView):
    """
    The class operates the process of the validation of a user.
    """
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('index')

    def get_redirect_url(self):
        """
        Defines the URL to open after the login.
        """
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful login.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has logged in successfully.')
        return result


class AccountLogoutView(LogoutView):
    """
    The class operates the process of the validation of a user.
    """
    template_name = 'accounts/logout.html'

    def get_context_data(self, **kwargs):
        messages.success(self.request, 'You have been logged out successfully.')
        return super().get_context_data(**kwargs)


class AccountUpdateView(UpdateView):
    """
    The class processes the edit of user's data.
    """
    model = User
    template_name = 'accounts/profile.html'
    success_url = reverse_lazy('index')
    form_class = AccountUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful profile update.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has been successfully updated.')
        return result


class AccountAndProfileUpdateView(LoginRequiredMixin, ProcessFormView):
    """
    Update data on a user and his/her profile.
    """

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountAndProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='accounts/profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountAndProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


class AccountPasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    """
    Change the account password.
    """
    model = User
    template_name = 'accounts/password.html'
    success_url = reverse_lazy('index')
    form_class = AccountPasswordUpdateForm

    def form_valid(self, form):
        """
        Modifies the displayed massage that confirms a successful password alteration.
        """
        result = super().form_valid(form)
        messages.success(self.request, f'The user <<{self.request.user}>> has successfully changed the password.')
        return result


class AccountDeleteView(LoginRequiredMixin, DeleteView):
    """
    Delete an Account.
    """

    model = User
    success_url = reverse_lazy('index')
    template_name = 'accounts/delete.html'

    def get_context_data(self, **kwargs):
        messages.success(self.request, 'Your account has been successfully deleted.')
        return super().get_context_data(**kwargs)

    def get_object(self, queryset=None):
        """
        Preserves from IntegrityErrors caused by creating an instance of 'core.Logger'
        in the 'core.PerfTrackerMiddleware' (field 'user'). [middlewares.py]
        """
        return self.request.user
