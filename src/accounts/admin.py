from accounts.models import Profile

from django.contrib import admin  # noqa

# Register your models here.

admin.site.register(Profile)
