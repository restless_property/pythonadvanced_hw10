import os

from accounts.models import Profile

from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from lms.settings import MEDIA_ROOT


@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(pre_delete, sender=User)
def delete_profile_picture(sender, instance, **kwargs):
    if instance.profile.image.name != Profile._meta.get_field('image').get_default():
        # os.remove(os.path.join(MEDIA_ROOT, instance.profile.image.path))
        instance.profile.image.delete()


@receiver(post_save, sender=User)
def delete_old_picture(sender, instance, **kwargs):
    old_image_path = Profile.objects.get(id=instance.profile.id).image.path
    # in case the picture won't change and the profile is saved this \/ line prevents the delete of the current image
    if instance.profile.image.name != Profile.objects.get(id=instance.profile.id).image.name:
        os.remove(os.path.join(MEDIA_ROOT, old_image_path))
