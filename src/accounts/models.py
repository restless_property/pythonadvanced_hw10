from django.contrib.auth.models import User
from django.db import models  # noqa

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(
        to=User,  # settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    image = models.ImageField(null=True, default='pics/default.png', upload_to='pics/')
    interests = models.CharField(max_length=128, null=True)
