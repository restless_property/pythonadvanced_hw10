"""
The module contains auxiliary functions used by the 'views.py' module
(App 'students').
"""


def format_table(records):
    """
    Formats the content of a 'Python' collection.

    :param records: (collection) a 'Python' collection
    :return response: (str) a formatted content of the collection
    """
    response = ""
    for line in records:
        link = '<a href="{% url \'students: update\' line.id %}">[UPDATE]</a>'
        del_link = '<a href="{% url \'students: delete\' line.id %}">[DELETE]</a>'
        response += "<br>" + link + del_link + '&nbsp;|&nbsp;' + str(line)
    return response if response else '&ltNo results for your query&gt'


def repack_query_set(qs):
    qs_values = qs.values()
    dict_list = list(qs_values)
    data = [voc.values() for voc in dict_list]
    return data
