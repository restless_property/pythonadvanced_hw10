from django.contrib import admin  # noqa

from students.models import Student

# Register your models here.


class StudentAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'first_name', 'last_name', 'age']
    list_filter = ['id', 'first_name', 'last_name', 'age']  # <&>-filter  # TODO: adds more SQL requests. Optimize?
    search_fields = ['id', 'first_name', 'last_name', 'age']  # <|>-filter
    list_select_related = ['group', 'led_group']


admin.site.register(Student, StudentAdmin)
