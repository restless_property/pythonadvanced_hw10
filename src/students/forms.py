"""Module 'forms' (App 'students')"""

from django.core.exceptions import ValidationError
from django.forms import ModelForm

import django_filters

from students.models import Student


class StudentFilter(django_filters.FilterSet):

    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = {
            'first_name': ['exact', 'startswith', 'icontains'],
            'last_name': ['exact', 'startswith', 'icontains'],
            'age': ['exact', 'lt', 'gt']
        }


class StudentBaseForm(ModelForm):

    class Meta:
        model = Student
        # fields = ['first_name', "last_name", "age"]
        fields = '__all__'

    def as_p_br(self):
        """
        Return this form rendered as HTML <p>s (with extra <break>).
        """
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s <br/> %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean_email(self):
        email = self.cleaned_data['email']
        email_not_unique = Student.objects.filter(email=email).exclude(id=self.instance.id).exists()
        if email_not_unique:
            raise ValidationError("This email is not unique.")
        return email

    def clean(self):
        result = super().clean()
        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError('The enroll date is later than the graduate date')

        return result


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):

    class Meta(StudentBaseForm.Meta):
        model = Student
        fields = '__all__'
        # fields = ['first_name', "last_name", "age"]
        # exclude = ['age']

    def clean_group(self):
        group = self.cleaned_data['group']
        if hasattr(self.instance, 'led_group') and self.instance.led_group != group:
            raise ValidationError(
                f'Student {self.instance.get_full_name()} is a headman in another group ({self.instance.led_group}).'
                ' Please, fix it and try again.')
        return group
