"""
The module is a realization for students of groups in LMS.
"""
import datetime
import random

from core.models import Person
from core.validators import validate_phone_number

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from faker import Faker

from groups.models import Group


# Create your models here.


class Student(Person):
    """
    The class contains class attributes only
    and covers personal information on a student of a group.
    """

    age = models.IntegerField(null=True,
                              default=21,
                              validators=[
                                          MinValueValidator(10),
                                          MaxValueValidator(100)
                                          ])
    # email = models.EmailField(max_length=64,
    #                           validators=[
    #                                       validate_email_for_prohibited_domain
    #                                       ])
    # email = models.EmailField(max_length=64)
    phone_number = models.CharField(max_length=20,
                                    null=False,
                                    validators=[
                                        validate_phone_number
                                    ])
    enroll_date = models.DateField(null=False, default=datetime.date.today)
    graduate_date = models.DateField(null=False, default=datetime.date.today())
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def __str__(self):
        """
        The method defines printable representation
        of instances of the class 'Student'
        (for rows of the Table).

        :return: (str) the name and the age of a student
        """
        return f'#ID {self.id}: {self.get_full_name()}'

    def get_info(self):
        return f'Name: {self.first_name} {self.last_name} - Age: {self.age} - ' \
               f'E-mail: {self.email} - Number: {self.phone_number} - ' \
               f'Enroll date: {self.enroll_date} - Graduate date: {self.graduate_date}'

    @classmethod
    def generate_students(cls, count):
        """
        The method generates a specified number of students
        and adds them to a DB as records.

        :param count: (int) a number of students to generate
        """
        if count <= 0:
            raise ValueError("Cannot add a negative or zero number of records into the DB.")
        faker = Faker()
        groups = list(Group.objects.all()) or [None]
        if not groups:
            print("No groups are included into the DB at the moment")
        for _ in range(count):
            obj = cls(
                      first_name=faker.first_name(),
                      last_name=faker.last_name(),
                      age=random.randint(16, 60),
                      group=random.choice(groups) if groups else None
                     )
            obj.save()
