"""
Django module with view-functions (App 'students').
"""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q  # noqa
from django.http import HttpResponse, HttpResponseRedirect  # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentCreateForm, StudentFilter, StudentUpdateForm
from students.models import Student

# Create your views here.


def get_students(request):
    """
    This method outputs records from the DB table 'students_student'
    according to an SQL request depending on filter parameters.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered HTML code

    Local variables:
    students: (QuerySet) records of the table 'students_student'
                         according to filter parameters
    params: (list) admissible SQL filters
    param_name: (str) a filter from the list of filters
    param_value: (str) a value input as a parameter
    param_elems: (list) separated multiple values input as a parameter
    param_elem: (str) separate value from the list
                      *been input as a parameter
    or_filter: (Q-utility) 'an empty object for 'OR' SQL filtration
    form: (str) an HTML template to render into the web-page
    result: (str) formatted table records
    """
    students = Student.objects.all().select_related('group', 'led_group')

    # --- Substitutes [netto filter] ---
    # params = ['first_name',
    #           'first_name__startswith',
    #           'first_name__endswith',
    #           'last_name',
    #           'age',
    #           'age__gt']
    #
    # # --- Current 'AND/OR' realization ---
    # for param_name in params:
    #     param_value = request.GET.get(param_name)
    #     if param_value:
    #         param_elems = param_value.split(',')
    #         if param_elems:
    #             or_filter = Q()
    #             for param_elem in param_elems:
    #                 or_filter |= Q(**{param_name: param_elem})
    #             students = students.filter(or_filter)
    #         else:
    #             students = students.filter(**{param_name: param_value})
    filt = StudentFilter(
        data=request.GET,
        queryset=students
    )
    return render(request=request,
                  template_name='students/students_list.html',
                  context={'filter': filt})

    # ---                                                     ---
    # --- Obsolete 'AND'-only realization ---
    # for param_name in params:
    #     param_value = request.GET.get(param_name)
    #     if param_value:
    #         students = students.filter(**{param_name: param_value})
    # ---           ---
    # --- Substituted [brutto filter] ---
    # first_name = request.GET.get('first_name')
    # if first_name:
    #     students = students.filter(first_name=first_name)
    # last_name = request.GET.get('last_name')
    # if last_name:
    #     students = students.filter(last_name=last_name)
    # age = request.GET.get('age')
    # if age:
    #     students = students.filter(age=age)
    # ---
    # --- Substituted --- #
    # form = """
    #         <form>
    #           <label >First name:</label><br>
    #           <input type="text" name="first_name" placeholder="Enter your first name"><br>
    #
    #           <label >Last name:</label><br>
    #           <input type="text" name="last_name" placeholder="Enter your last name"><br>
    #
    #           <label >Age:</label><br>
    #           <input type="number" name="age" placeholder="Enter your age"><br><br>
    #
    #           <input type="submit" value="Submit">
    #         </form>
    #         """
    # result = format_table(students)
    # return HttpResponse(form + result)
    # ---  --- #


def create_students(request):
    """
    This method generates new students
    and adds them into the 'students_student' table of the DB.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    form: (user's Modelform) parameters to post and input into the DB
    html_template: (str) HTML code to be rendered into the web-page
    result: (str) formatted HTML code with the values of parameters
                  rendered as paragraphs
    """
    # --- Substitutes [ModelForm] ---
    if request.method == 'POST':
        form = StudentCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('students:list'))
    # ---       ---
    elif request.method == 'GET':
        form = StudentCreateForm()
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request=request,
                  template_name='students/students_create.html',
                  context={'form': form})
    # --- Substituted [no ModelForm] ---
    # first_name = request.GET.get('first_name')
    # last_name = request.GET.get('last_name')
    # age = request.GET.get('age')
    #
    # student = Student(first_name=first_name,
    #                   last_name=last_name,
    #                   age=age)
    # student.save()
    # return HttpResponseRedirect('/students/')
    # ---       ---


def update_students(request, pk):
    """
    This method updates information on a student
    the table id of which is indicated as a parameter

    :param request: (str) a request received by the server from a client
    :param pk: (int) an id of a group in the table 'students_student'
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    student: (QS) a group in the table with the specified id
    form: (user's Modelform) parameters to post and input into the DB
                             reciprocally rendered into HTML and 'Python' code
    """
    student = Student.objects.get(id=pk)
    if request.method == 'POST':
        form = StudentUpdateForm(data=request.POST, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('students:list'))
    elif request.method == 'GET':
        form = StudentUpdateForm(instance=student)
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request=request,
                  template_name='students/students_update.html',
                  context={'form': form})
    # html_template = """
    #                         <form method='post'>
    #                           {}<br/>
    #                           <input type="submit" value="Update students">
    #                         </form>
    # #                         """
    # result = html_template.format(form.as_p_br())
    # return HttpResponse(result)


def delete_student(request, pk):
    student = get_object_or_404(Student, id=pk)
    if request.method == 'POST':
        student.delete()
        return HttpResponseRedirect(reverse('students:list'))
    return render(request=request,
                  template_name='students/students_delete.html',
                  context={'student': student})


class StudentListView(LoginRequiredMixin, ListView):  # TODO: ADD COLLAPSING ELEMENTS IN THE TEMPLATE
    model = Student
    template_name = 'students/students_list.html'
    paginate_by = 10
    context_object_name = 'students'
    login_url = reverse_lazy('accounts:login')

    def get_filter(self):
        return StudentFilter(
               data=self.request.GET,
               queryset=self.model.objects.all()
               )

    def get_queryset(self):
        return self.get_filter().qs.select_related('group', 'led_group')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        # context['students'] = self.get_filter().qs.select_related('group', 'led_group')  # replaced to get_qs()
        return context


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students/students_create.html'
    login_url = reverse_lazy('accounts:login')


class StudentUpdateView(LoginRequiredMixin, UpdateView):

    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students/students_update.html'
    login_url = reverse_lazy('accounts:login')
    # pk_url_kwarg = 'id'
    # fields = "__all__"


class StudentDeleteView(LoginRequiredMixin, DeleteView):

    model = Student
    success_url = reverse_lazy('students:list')
    template_name = 'students/students_delete.html'
    login_url = reverse_lazy('accounts:login')
