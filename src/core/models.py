"""
Basic models for the app. "core"
"""
from core.validators import validate_email_for_prohibited_domain

from django.contrib.auth.models import User

from django.db import models  # noqa

# Create your models here.


class Person(models.Model):
    """
    Represents common features any person possesses
    (in the context of the project's purposes).
    """

    class Meta:
        """
        Prevents the creation of a separate abstract table 'core_persons'.
        All the features will transform into the fields of the inheriting models.
        """

        abstract = True

    first_name = models.CharField(max_length=64, null=False, verbose_name="First Name")
    last_name = models.CharField(max_length=90, null=False, verbose_name="Last Name")
    email = models.EmailField(max_length=64,
                              validators=[
                                          validate_email_for_prohibited_domain
                                          ])

    def get_full_name(self):
        """
        Returns the full name of a person.
        """
        return f'{self.first_name} {self.last_name}'


class Logger(models.Model):
    user = models.ForeignKey(to=User, null=True, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)

    def __str__(self):
        return f'Path: "{self.path}" * ' \
               f'Date of the request: {self.create_date} * ' \
               f'Action type: {self.info}'

    def get_info(self):
        return f'User: "{self.user}" * ' \
               f'Path: "{self.path}" * ' \
               f'Date of the request: {self.create_date} * ' \
               f'Productivity: {self.execution_time} s. * ' \
               f'Action type: {self.info} * '
        # f'Parameters: {self.query_params} * ' --> odd representation with <\>

    def get_params(self):
        return self.query_params

    @classmethod
    def make_log(cls, user, path, create_date, execution_time, query_params, info):
        obj = cls(user=user,
                  path=path,
                  create_date=create_date,
                  execution_time=execution_time,
                  query_params=query_params,
                  info=info,
                  )
        obj.save()
