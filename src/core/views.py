from django.shortcuts import render  # noqa

# Create your views here.


def index(request):
    """
    The function renders the index HTML page.
    """
    return render(
        request,
        'index.html',
    )
