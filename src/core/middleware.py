import datetime
import time

from core.models import Logger


class PerfTrackerMiddleware:

    ADMISSIBLE_PARAMS = ["number", "groups",
                         "course_name", "group", "teachers", "difficulty",
                         "group_name", "mentor", "average_score", "headman",
                         "first_name", "last_name", "email", "phone_number", "enroll_date", "graduate_date",
                         "age", "date_of_birth", "mobile_number", "education", "exp_years"]

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        start = time.time()
        response = self.get_response(request)
        execution_time = time.time() - start
        path = request.path
        print(f'The request for the URL {path} has been processed in {execution_time} seconds.')

        if request.method == 'GET':
            params = request.GET
            query_params = {key: value for key, value in params.items() if bool(value)} or None
        elif request.method == 'POST':
            params = request.POST
            query_params = {}
            for admissible_param in PerfTrackerMiddleware.ADMISSIBLE_PARAMS:
                if admissible_param in params:
                    query_params.update({admissible_param: params[admissible_param]})
        else:
            query_params = {}
        Logger.make_log(user=request.user if request.user.pk else None,
                        path=path,
                        create_date=datetime.date.today(),
                        execution_time=execution_time,
                        query_params=query_params,
                        info='')
        return response
        # ------------ CANCELLED ------------ #

        # pk = request.resolver_match.kwargs.get('pk', '0')  # AttributeError:
        #                                                      'NoneType' object has no attribute 'kwargs'
        # options = {'/admin/': "Open the administrative panel",
        #            reverse('index'): "Return to the index page",
        #            reverse('accounts:registration'): "Register a new account",
        #            reverse('accounts:login'): "Log in",
        #            reverse('accounts:logout'): "Log out",
        #            reverse('accounts:profile'): "View/update profile",
        #            reverse('accounts:password'): "Change profile password",
        #            reverse('courses:filter'): "Search a course",
        #            reverse('courses:new'): "Create a new course",
        #            f'/courses/update/{pk}': "Edit a course",
        #            f'/courses/delete/{pk}': "Delete a course",
        #            reverse('groups:filter'): "Search a group",
        #            reverse('groups:new'): "Create a new group",
        #            f'/groups/update/{pk}': "Edit a group",
        #            f'/groups/delete/{pk}': "Delete a group",
        #            reverse('students:list'): "Search a student",
        #            reverse('students:create'): "Create a new student",
        #            f'/students/update/{pk}': "Edit a student",
        #            f'/students/delete/{pk}': "Delete a student",
        #            reverse('teachers:filter'): "Search a teacher",
        #            reverse('teachers:new'): "Create a new teacher",
        #            f'/teachers/update/{pk}': "Edit a teacher",
        #            f'/teachers/delete/{pk}': "Delete a teacher"
        #            }
        # params = urlencode(request.GET) if request.method == 'GET' else urlencode(request.POST)

        # ------  POSSIBLE ERRORS: KeyError, AttributeError: csrf-token ----- #
        # params.pop('csrfmiddlewaretoken')  # AttributeError: this QueryDict instance is immutable
        # query_params = {key: value for key, value in params.items() if bool(value)} or None

        # If there was/were (a) param(s) in var <params> and a csrf-token among them

        # if query_params is not None and query_params.get('csrfmiddlewaretoken'):
        #     query_params.pop('csrfmiddlewaretoken')

        # ------                                                            ----- #
        # return response
        # ------------           ------------ #
