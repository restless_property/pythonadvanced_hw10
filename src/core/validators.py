"""
This module validates values input through HTML forms
as parameters of the corresponding Tables ('models' of Apps).
"""

import datetime
import re

from dateutil.relativedelta import relativedelta

from django.core.exceptions import ValidationError


def validate_email_for_prohibited_domain(email):
    """
    The method validates email for prohibited domains.
    """
    if email:
        BLACKLIST = ['gov.ru', 'yandex.ru', 'mail.ru', 'bk.ru', 'ok.ru']   # noqa
        _, _, email_domain = email.partition('@')
        if email_domain in BLACKLIST:
            raise ValidationError('Prohibited domain.')
    return email


def validate_phone_number(phone_number):
    """
    The method validates the correspondence
    between the input phone number
    and the admissible phone format.
    """
    SHORT_LENGTH = 13  # noqa
    pattern = '^(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d$'  # noqa
    if phone_number and not re.match(pattern, phone_number):
        raise ValidationError('Phone number is not admissible. Must be [+**](***)***-****')
    if phone_number and len(phone_number) == SHORT_LENGTH:
        phone_number = '+38' + phone_number
    return phone_number


def validate_phone_number_v2(phone_number):
    """
    The method validates the applicability of values
    transferred as the parameter 'mobile_number'.
    """
    SHORT_LENGTH = 15  # noqa
    pattern = '^(\(\d\d\d\)|\+\d\d \(\d\d\d\)) \d\d\d\-\d\d\-\d\d$'  # noqa
    if phone_number and not re.match(pattern, phone_number):
        raise ValidationError('Mobile number is not admissible. Must be [+**] (***) ***-**-**')
    if phone_number and len(phone_number) == SHORT_LENGTH:
        phone_number = '+38 ' + phone_number
    return phone_number

# ----- #


# def validate_age(age, person, min_age=21, max_age=150):
def validate_age(age, min_age=21, max_age=150):
    """
    The method validates the applicability of values
    transferred as the parameter 'age'.
    """
    if age and not (min_age <= age <= max_age):
        raise ValidationError(f'A lecturer cannot be younger than {min_age} and older than {max_age} y.o.')
        # raise ValidationError(f'A {person} cannot be younger than {min_age} and older than {max_age} y.o.')
    return age


def validate_age_for_students(age):
    return validate_age(age=age, min_age=18, max_age=150)
    # person = "student"
    # return validate_age(age=age, person=person, min_age=18, max_age=150)


def validate_age_for_teachers(age):
    return validate_age(age=age, min_age=21, max_age=50)
    # person = "lecturer"
    # return validate_age(age=age, person=person, min_age=21, max_age=50)

# ----- #


# def validate_date_of_birth(dob, person min_dt=21, max_dt=150):
def validate_date_of_birth(dob, min_dt=21, max_dt=150):
    """
    The method validates the applicability of values
    transferred as the parameter 'date_of_birth'.
    """
    mini = datetime.date.today() - relativedelta(years=min_dt)
    maxi = datetime.date.today() - relativedelta(years=max_dt)
    if dob and not (maxi <= dob <= mini):
        raise ValidationError(f'A lecturer cannot be younger than {min_dt} and older than {max_dt} y.o.')
        # raise ValidationError(f'A {person} cannot be younger than {min_dt} and older than {max_dt} y.o.')
    return dob


def validate_date_of_birth_students(dob):
    return validate_date_of_birth(dob=dob, min_dt=18, max_dt=150)
    # person = "student"
    # return validate_date_of_birth(dob=dob, person=person, min_dt=18, max_dt=150)


def validate_date_of_birth_teachers(dob):
    return validate_date_of_birth(dob=dob, min_dt=21, max_dt=50)
    # person = "lecturer"
    # return validate_date_of_birth(dob=dob, person=person, min_dt=21, max_dt=50)
