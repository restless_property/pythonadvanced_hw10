"""
This module is the realization for the table of classrooms in LMS.
(App. 'classrooms').
"""

import random

from classrooms.model_tools import figure_remaining_classrooms

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.


class Classroom(models.Model):
    """
    The model represents a classroom to hold a lecture in.

    Class fields:
    number: (integerfield) the number of a classroom
    groups: (foreignkey: M2M) groups that attend a classroom during lectures
    """
    number = models.IntegerField(null=False, validators=[
                                             MinValueValidator(1),
                                             MaxValueValidator(999)
                                             ])
    groups = models.ManyToManyField(
        to='groups.Group',
        related_name='classrooms'
    )

    def __str__(self):
        """
        A printable representation of one row
        inside the table 'classrooms_classroom'
        as an instance of the class 'Classroom'.

        :return output: (str) profile data on a classroom
        """
        output = f"Number: {self.number}"
        if self.groups.exists():
            output += f' * Groups: {self.groups}'
        return output

    @classmethod
    def generate_classrooms(cls, count: int):
        """
        The method generates a specified number of classrooms
        and adds them to a DB as records.

        :param count: (int) a number of classrooms to generate

        Local variables:
        _: (int) unused sequential numbers of the counter
        remaining_classrooms: (set) unoccupied classrooms available for their utilization
        obj: (Classroom) an instance of the corresponding class
        """
        if count <= 0:
            raise ValueError("Cannot add a negative or zero number of records into the DB.")

        for _ in range(count):
            remaining_classrooms = figure_remaining_classrooms(Classroom)
            try:
                obj = cls(
                    number=random.choice(list(remaining_classrooms))
                    )
                obj.save()
            except IndexError:
                print("All the available classrooms are already occupied.")
                break
