"""
This module contains utility functions for the module 'models' (App. "classrooms").
"""


def figure_remaining_classrooms(model):
    """
    This function determines unoccupied classrooms
    among the set of available ones.

    :param: model: (class) the table to extract the occupied classrooms from
    :return: (set) classrooms not yet occupied (not included into the table)
                   and accessible for addition into the table

    Local variables:
    available_classrooms: (set) all the rooms physically available for the service provider
                                to handle the lessons in
    model_values: (dict) fields as keys and their values as values
                         for all the instances of the table (model)
    occupied_classrooms: (list) classrooms already included into the table "classrooms_classroom"
    """
    available_classrooms = set(range(100, 200))
    model_values = model.objects.values()
    occupied_classrooms = [classroom['number'] for classroom in model_values]
    return available_classrooms.difference(set(occupied_classrooms))
