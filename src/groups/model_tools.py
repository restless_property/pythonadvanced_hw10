"""
The module represents subsidiary technical code used in the 'models.py'
(App 'groups').
"""
import datetime
import random

from dateutil.relativedelta import relativedelta


def generate_group(faked):
    """
    The method makes a dictionary of fake data on a group.

    :param faked: (Faker) an instance of the class 'Faker' from the module 'faker'
                          used to make some fake data
    :return group: (dict) generated faked data on a group
    group_name, subject_name, lecturer, mentor, headman, group
    number_of_students, average_score: (str/int/objects) fake parameters of a group
    group_names: (dict) possible names for a group with relation to the subject
    """
    group_names = {f"Java Course [{make_random_date()}]": ['Introduction Java', 'Java Elementary', 'Java Enterprise'],
                   f"Web layout [{make_random_date()}]": ['Front-End Basic', 'Front-End Pro', 'React'],
                   f"PHP Development [{make_random_date()}]": ['PHP', 'Basic PHP'],
                   f"Python Development [{make_random_date()}]": ['Introduction Python', 'Advanced Python'],
                   f"DevOps Engineering [{make_random_date()}]": ['DevOps'],
                   f"Data Science [{make_random_date()}]": ['Machine Learning'],
                   f"QA and Automation [{make_random_date()}]": ['QA', 'QA Automation'],
                   f"IT Management [{make_random_date()}]": ['Project Management', 'HR IT', 'Business Analysis'],
                   f"IT Design [{make_random_date()}]": ['Design', 'UI/UX Design']}
    group_name = random.choice(list(group_names))
    # subject_name = random.choice(group_names[group_name])
    # lecturer = f'{faked.first_name()} {faked.last_name()}'
    mentor = f'{faked.first_name()} {faked.last_name()}'
    # number_of_students = random.randint(14, 30)
    average_score = random.randint(70, 100)
    headman = None
    group = {'group_name': group_name,
             # 'subject_name': subject_name,
             # 'lecturer': lecturer,
             'mentor': mentor,
             # 'number_of_students': number_of_students,
             'average_score': average_score,
             'headman': headman
             }
    return group


def make_random_date():
    """
    This function is used to generate a random date (in format :'M, Y')
    in range from the current date to up to one year into the past

    :return: (str) the moth and the year of the random date

    Local variables:
    start_date: (datetime.date) a date one year before the current one
    end_date: (datetime.date) current date
    days_delta: (int) an amount of days between the two aforementioned dates
    days_random: (int) an arbitrary number of days
                       to substrate from the current date

    """
    start_date = datetime.date.today() - relativedelta(years=1)
    end_date = datetime.date.today()
    days_delta = (end_date - start_date).days
    days_random = random.randrange(days_delta)
    date_random = start_date + datetime.timedelta(days=days_random)
    return date_random.strftime("%B, %Y")
