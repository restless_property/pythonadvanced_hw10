"""
Django module with view-functions (App 'groups').
"""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm, GroupFilter, GroupUpdateForm
from groups.models import Group
from groups.tools import repack_query_set

# Create your views here.
from students.models import Student  # noqa


def get_groups(request):
    """
    This method outputs records from the DB table 'groups_group'
    according to an SQL request depending on filter parameters.

    :param request: (str) a request received by the server from a client
    :return groups: (QS) fetched from the table records on groups

    Local variables:
    groups: (QuerySet) records of the table 'groups_group'
                       according to filter parameters
    params: (list) admissible SQL filtering parameters
    param_name: (str) one filtering parameter from the list
    param_value: (str) a value input as a certain parameter (param_name)
    param_merged: (str) one or several values of the parameter input by a client
    param_elems: (list) parameter values stripped of redundant space symbols
    param_elem: (str) one separate value from the list of parameter values
    or_filter: (Q) a filtering utility which
                   applies the logical operator "OR" to SQL requests


    """
    groups = Group.objects.all()
    params = ['group_name',
              'group_name__startswith',
              'group_name__endswith',
              'subject_name',
              'subject_name__startswith',
              'subject_name__endswith',
              'lecturer',
              'lecturer__startswith',
              'lecturer__endswith',
              'mentor',
              'mentor__startswith',
              'mentor__endswith',
              'number_of_students',
              'number_of_students__gt',
              'number_of_students__gte',
              'number_of_students__lt',
              'number_of_students__lte',
              'average_score',
              'average_score__gt',
              'average_score__gte',
              'average_score__lt',
              'average_score__lte'
              ]
    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_merged = param_value.split(',')
            param_elems = list(map(lambda x: x.strip(), param_merged))
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    if param_name == 'number_of_students' or param_name == 'average_score':
                        or_filter |= Q(**{param_name: param_elem})
                    else:
                        or_filter |= Q(**{f'{param_name}__contains': param_elem})
                groups = groups.filter(or_filter)
            else:
                groups = groups.filter(**{param_name: param_value})
    return groups


def render_groups_as_table(request):
    """
    This method renders data on groups as an HTML table.

    :param request: (str) a request received by the server from a client
    :result: (str) formatted HTML code with the values of parameters

    Local variables:
    groups: (QS) records of the table 'groups_group'
                 according to filter parameters
    data: (list) results of an SQL query repacked into a list
    headers: (list) a list of the names of the table columns
    """
    groups = get_groups(request)
    data = repack_query_set(groups)
    try:
        headers = [f.verbose_name for f in groups[0]._meta.fields]
    except IndexError:
        headers = []
    return render(request, "groups/groups_get.html",
                  {"headers": headers, "data": data})


def render_groups_as_list(request):
    """
    This method renders data on groups as HTML paragraphs.

    :param request: (str) a request received by the server from a client
    :result: (str) formatted HTML code with the values of parameters

    Local variables:
    groups: (QS) records of the table 'groups_group'
                 according to filter parameters
    """
    groups = (get_groups(request))
    return render(request=request,
                  template_name='groups/groups_list.html',
                  context={'records': groups})


def get_groups_with_filter(request):
    """
    This method outputs records from the DB table 'groups_group'
    according to an SQL request formed with a 'django_filters.FilterSet'.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered HTML code

    Local variables:
    groups: (QS) all the records from the table 'groups_group' of the DB.
    filt: ('django_filters.FilterSet') a class that renders preferred attributes of
                                       instances of the model "Group" into HTML code
    """
    groups = Group.objects.all().select_related("headman", "subject").prefetch_related('students', 'teachers')
    # headers = []
    # if groups:
    #     headers = [f.verbose_name for f in Group.objects.last()._meta.fields]
    #     headers.extend(["🖉", "🗑"])
    filt = GroupFilter(
        data=request.GET,
        queryset=groups)
    return render(request=request,
                  template_name='groups_filter/groups_get_filter.html',
                  context={'filter': filt})
    # context={'filter': filt,'headers': headers})


def create_groups(request):
    """
    This method generates a new group
    and adds it into the 'groups_group' table of the DB.

    :param request: (str) a request received by the server from a client
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    form: (user's Modelform) parameters to post and input into the DB
                             reciprocally rendered into HTML and 'Python' code
    html_template: (str) HTML code to be rendered into the web-page
    result: (str) formatted HTML code with the values of parameters
                  rendered as paragraphs/redirects to the table 'groups_group'
    """
    if request.method == 'POST':
        form = GroupCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:filter'))
    elif request.method == 'GET':
        form = GroupCreateForm()
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    # return render(request, 'groups/groups_create.html', {"fields": form.as_p_br()})  # without 'FilterSet'
    return render(request, 'groups/groups_create.html', {"form": form})


def update_group(request, pk):
    """
    This method updates information on a group
    the table id of which is indicated as a parameter

    :param request: (str) a request received by the server from a client
    :param pk: (int) an id of a group in the table 'groups_group'
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    group: (Group) a group in the table with the specified id
    form: (user's Modelform) parameters to post and input into the DB
                             reciprocally rendered into HTML and 'Python' code
    """
    group = get_object_or_404(Group, id=pk)
    if request.method == 'POST':
        form = GroupUpdateForm(data=request.POST, instance=group)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:filter'))
    elif request.method == 'GET':
        form = GroupUpdateForm(instance=group)
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request=request,
                  template_name='groups/groups_update.html',
                  # context={'fields': form.as_p_br(),  # without 'FilterSet'
                  context={'form': form,
                           'id': pk,
                           # 'group': group})
                           'group': group,
                           'students': group.students.select_related('group', 'led_group').all(),
                           'teachers': group.teachers.select_related('group').all()})


def delete_group(request, pk):
    """
    This method deletes a specified group
    from the relevant table in the DB.

    :param request: (str) a request received by the server from a client
    :param pk: (int) an id of a group in the table 'groups_group'
    :return: (str) formed response with rendered formatted HTML code

    Local variables:
    group: (Group) a group in the table with the specified id

    """
    group = get_object_or_404(Group, id=pk)
    if request.method == 'POST':
        group.delete()
        return HttpResponseRedirect(reverse('groups:filter'))
    return render(request=request,
                  template_name='groups/groups_delete.html',
                  context={'group': group})


class GroupFilterView(LoginRequiredMixin, ListView):
    """
    Searches and filters groups.
    """
    model = Group
    template_name = 'groups_filter/groups_get_filter.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 5
    context_object_name = 'groups'

    def get_filter(self):
        return GroupFilter(
               data=self.request.GET,
               queryset=self.model.objects.all()
               )

    def get_queryset(self):
        return self.get_filter().qs.select_related("headman", "subject").prefetch_related("students", "teachers")

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        # context['groups'] = self.get_filter().qs.\
        #     select_related("headman", "subject").prefetch_related("students", "teachers")
        return context


class GroupCreateView(LoginRequiredMixin, CreateView):
    """
    Create a new group.
    """
    model = Group
    form_class = GroupCreateForm
    success_url = reverse_lazy('groups:filter')
    template_name = 'groups/groups_create.html'
    login_url = reverse_lazy('accounts:login')


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    """
    Update a group.
    """

    model = Group
    form_class = GroupUpdateForm
    success_url = reverse_lazy('groups:filter')
    template_name = 'groups/groups_update.html'
    login_url = reverse_lazy('accounts:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.get_object().id
        # context['group'] = self.get_object()
        context['students'] = self.get_object().students.select_related('group', 'led_group').all()
        context['teachers'] = self.get_object().teachers.select_related('group').all()
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    """
    Delete a group.
    """

    model = Group
    success_url = reverse_lazy('groups:filter')
    template_name = 'groups/groups_delete.html'
    login_url = reverse_lazy('accounts:login')
