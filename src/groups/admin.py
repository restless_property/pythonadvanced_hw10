"""
The administrative module for the app. 'groups'.
"""

from django.contrib import admin

# Register your models here.
from groups.models import Group

from students.models import Student

from teachers.models import Teacher


class StudentTable(admin.TabularInline):
    model = Student
    fields = ['first_name', 'last_name', 'age']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class TeachersTable(admin.TabularInline):
    model = Teacher
    fields = ['first_name', 'last_name', 'age']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class GroupAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['group_name', 'average_score', 'headman']
    filter_fields = ['group_name', 'average_score', 'headman']  # <&>-filter
    search_fields = ['group_name', 'average_score', 'headman']  # <|>-filter
    list_select_related = ['headman', 'subject']

    inlines = [StudentTable, TeachersTable]


admin.site.register(Group, GroupAdmin)
