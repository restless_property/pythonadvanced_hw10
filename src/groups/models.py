"""
The module is a realization for the table of groups in LMS.
"""

from django.db import models

from faker import Faker

from groups.model_tools import generate_group

# Create your models here.


class Group(models.Model):
    """
    The class contains class attributes
    for the rows of the table 'groups_group'
    and covers on a group.

    Methods:
    generate_teachers: generates a range of fake groups
    """

    group_name = models.CharField(max_length=90, null=False, verbose_name="Group")
    # subject_name = models.CharField(max_length=90, null=False, verbose_name="Subject")
    # lecturer = models.CharField(max_length=90, null=True, verbose_name="Lecturer")
    # lecturer = models.OneToOneField(
    #                         to='teachers.Teacher',
    #                         null=True,
    #                         blank=True,
    #                         on_delete=models.SET_NULL,
    #                         related_name="group",
    #                         verbose_name="Lecturer")
    mentor = models.CharField(max_length=90, null=False, verbose_name="Mentor")
    # number_of_students = models.IntegerField(null=False, verbose_name="Students")
    average_score = models.IntegerField(null=True, blank=True, verbose_name="Avg. Score")
    headman = models.OneToOneField(
        to='students.Student',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="led_group",
        verbose_name="Headman"
        )

    def __str__(self):
        """
        A printable representation of one row
        inside the table 'groups_group'
        as an instance of the class 'Group'.

        :return: (str) profile data on a group
        """
        return f"#ID {self.id} {self.group_name}"

    def get_info(self):
        return f"Group: {self.group_name} || " \
               f"Lecturer: {self.get_teachers()} || Mentor: {self.mentor} || " \
               f"Headman: {self.headman} || " \
               f"Average score: {self.average_score}"

    def get_teachers(self):
        if self.teachers.first():
            return ', '.join([
                teacher.get_full_name()
                for teacher in self.teachers.all()
                ])
        else:
            return None

    @classmethod
    def generate_groups(cls, count: int):
        """
        The method generates a specified number of groups
        and adds them to a DB as records.

        :param count: (int) a number of groups to generate

        Local variables:
        fake: (Faker) an instance of the 'Faker' class
        _: (int) unused sequential numbers of the counter
        group: (dict) fake data on a group
        obj: (Group) an instance of the corresponding class
                     (one fake group)
        """
        if count <= 0:
            raise ValueError("Cannot add a negative or zero number of records into the DB.")
        fake = Faker()
        for _ in range(count):
            group = generate_group(fake)
            obj = cls(**group)
            obj.save()
            # ----- #
            # obj = cls(
            #     group_name=group['group_name'],
            #     # subject_name=group['subject_name'],
            #     lecturer=None,
            #     mentor=group['mentor'],
            #     # number_of_students=group['number_of_students'],
            #     average_score=group['average_score'],
            #     headman=None
            #     )
            # obj.save()
