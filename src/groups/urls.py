"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from debug_toolbar import settings  # noqa

from django.urls import path

# from groups.views import create_groups, delete_group, get_groups_with_filter, update_group
from groups.views import GroupCreateView, GroupDeleteView, GroupFilterView, GroupUpdateView  # noqa
from groups.views import render_groups_as_list, render_groups_as_table


app_name = "groups"

urlpatterns = [
    path('', render_groups_as_table, name='table'),
    # path('filter', get_groups_with_filter, name='filter'),
    # path('create', create_groups, name="new"),
    path('list', render_groups_as_list, name="list"),
    # path('update/<int:pk>', update_group, name='edit'),
    # path('delete/<int:pk>', delete_group, name='delete'),
    path('filter', GroupFilterView.as_view(), name='filter'),
    path('create', GroupCreateView.as_view(), name='new'),
    path('update/<int:pk>', GroupUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>', GroupDeleteView.as_view(), name='delete')

]
