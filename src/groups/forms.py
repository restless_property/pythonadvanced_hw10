"""
The module reciprocally processes HTML code and 'Python'-related one.
(App 'groups')
"""
from django.core.exceptions import ValidationError
from django.db import IntegrityError  # noqa
from django.forms import ChoiceField, ModelForm

import django_filters

from groups.models import Group

from students.models import Student  # noqa


class GroupFilter(django_filters.FilterSet):

    class Meta:
        model = Group
        fields = {
            'group_name': ["exact", "icontains"],
            # 'subject_name': ["exact", "icontains"],
            # 'lecturer': ["exact"],
            'teachers': ["exact"],
            'mentor': ["exact", "icontains"],
            'students': ["exact"],
            'average_score': ["exact", "gt", "gte", "lt", "lte"],
            "headman": ["exact"]
        }


class GroupBaseForm(ModelForm):
    """
    A form that converts "python"-related data types of instance attributes
    into relevant HTML representation.
    """

    class Meta:
        model = Group
        fields = "__all__"

    def as_p_br(self):
        """
        Returns the form rendered as HTML <p>s (with extra <break>'s).
        """
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s <br/> %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
            )

    def clean_number_of_students(self):
        """
        The method validates the applicability of values
        transferred as the parameter 'number_of_students'.
        """
        number = self.cleaned_data['number_of_students']
        if number and (number < 14 or number > 30):
            raise ValidationError('The group is supposed be '
                                  'of no less than 14 '
                                  'and no more than 30 students.')
        return number

    def clean_average_score(self):
        """
        The method validates the applicability of values
        transferred as the parameter 'average_score'.
        """
        score = self.cleaned_data['average_score']
        if score and (score < 50 or score > 100):
            raise ValidationError('Admissible diapason: from 50 to 100 points.')
        return score


class GroupCreateForm(GroupBaseForm):
    """
    Renders the code into HTML when a new group is created.
    """

    class Meta(GroupBaseForm.Meta):
        exclude = ['headman']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        group_students = [(st.id, st.get_full_name()) for st in self.instance.students.all()]
        self.fields['Headman'] = ChoiceField(choices=group_students, required=False)


#  -------------- COMPLICATED HEADMAN REALISATION --------------- #
# class GroupUpdateForm(GroupBaseForm):
#     """
#     Renders the code into HTML when a  group is updated.
#     """
#
#     class Meta(GroupBaseForm.Meta):
#         fields = ["group_name", "mentor",
#                   "average_score"]
#
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         group_students = [(st.id, st.get_full_name()) for st in self.instance.students.all()]
#         if group_students and self.instance.__dict__.get('headman'):
#             self.fields['Headman'] = ChoiceField(choices=group_students,
#                                                  initial=str(self.instance.headman.id),
#                                                  required=False
#                                                  )
#         else:
#             self.fields['Headman'] = ChoiceField(choices=group_students,
#                                                  required=False
#                                                  )
#
#     def save(self, commit=True):
#         pk = self.cleaned_data.get('Headman')
#         if pk:
#             headman = Student.objects.get(id=pk)
#             self.instance.headman = headman
#         return super().save(commit)


# ------------------ NOT FUNCTIONING PROPERLY ------------------------
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     students_qs = self.form_qs(Student)
    #     teachers_qs = self.form_qs(Teacher)
    #
    #     students = [(st.id, st.get_full_name()) for st in students_qs]
    #     teachers = [(teacher.id, teacher.get_full_name()) for teacher in teachers_qs]
    #     group_students = [(st.id, st.get_full_name()) for st in self.instance.students.all()]
    #     self.fields['Students'] = ChoiceField(choices=students, required=False)
    #     # (IF THERE IS NO HEADMAN IN THE GROUP,
    #     #  TRYING TO ACCESS THE 'id' field of a 'NoneType' instance raises an ATTRIBUTE.ERROR)
    #     try:
    #         self.fields['Headman'] = ChoiceField(choices=group_students,
    #                                              initial=str(self.instance.headman.id),
    #                                              required=False
    #                                              )
    #     except AttributeError:
    #         self.fields['Headman'] = ChoiceField(choices=group_students,
    #                                              required=False
    #                                              )
    #     self.fields['Lecturer'] = ChoiceField(choices=teachers, required=False)

    # def save(self, commit=True):
    #     # (IF NO HEADMAN IS SELECTED,
    #     #  THE CODE RAISES A VALUE.ERROR: wrong data type in the field 'id') [ --> line 122 of this module ]
    #     try:
    #         headman = Student.objects.get(id=self.cleaned_data['Headman'])
    #         self.instance.headman = headman
    #     except ValueError:
    #         pass
    #     return super().save(commit)

# ---------------------------
# Alternative working HEADMAN realisation


class GroupUpdateForm(GroupBaseForm):

    class Meta(GroupBaseForm.Meta):
        fields = ["group_name", "mentor",
                  "average_score", "headman"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['headman'].queryset = self.instance.students.all()
# ---------------------------
