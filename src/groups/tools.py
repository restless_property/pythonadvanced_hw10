"""
The module contains auxiliary functions used by the 'views.py' module
(App 'groups').
"""


def repack_query_set(qs):
    """
    The function reforms a QuerySet into a list
    to be used in HTML template rendering.

    :param qs: (QuerySet) a QS with parsed data
    :return data: (list) results of an SQL query
                         repacked into a list

    Local variables:
    qs_values: (QuerySet) values of the QS fields [dict]
    dict_list: (list) QS values [dict] repacked into a list
    voc: (dict) one dictionary-item inside the list of QS values
    """
    qs_values = qs.values()
    dict_list = list(qs_values)
    data = [voc.values() for voc in dict_list]
    return data
