from courses.models import Course

from django.contrib import admin


# Register your models here.


class CourseAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'course_name', 'group', 'difficulty']
    list_filter = ['id', 'course_name', 'group', 'difficulty']  # <&>-filter  # TODO: adds more SQL requests. Optimize?
    search_fields = ['id', 'course_name', 'group', 'difficulty']  # <|>-filter
    list_select_related = ['group']


admin.site.register(Course, CourseAdmin)
