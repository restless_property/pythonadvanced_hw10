"""
The module contains utilitarian functions for the module 'models'.
(App. 'courses').
"""

import random


def make_course():
    return random.choice(['Introduction Java', 'Java Elementary', 'Java Enterprise',
                          'Front-End Basic', 'Front-End Pro', 'React',
                          'PHP', 'Basic PHP',
                          'Introduction Python', 'Advanced Python',
                          'DevOps',
                          'Machine Learning',
                          'QA', 'QA Automation',
                          'Project Management', 'HR IT', 'Business Analysis',
                          'Design', 'UI/UX Design'
                          ])
