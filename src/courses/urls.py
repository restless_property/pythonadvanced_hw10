"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# from courses.views import create_course, delete_course, get_courses_with_filter, update_course
from courses.views import CourseCreateView, CourseDeleteView, CourseFilterView, CourseUpdateView

from debug_toolbar import settings  # noqa

from django.urls import path

app_name = "courses"


urlpatterns = [
    # path('filter', get_courses_with_filter, name='filter'),
    # path('create', create_course, name='new'),
    # path('update/<int:pk>', update_course, name='edit'),
    # path('delete/<int:pk>', delete_course, name='delete'),
    path('filter', CourseFilterView.as_view(), name='filter'),
    path('create', CourseCreateView.as_view(), name='new'),
    path('update/<int:pk>', CourseUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>', CourseDeleteView.as_view(), name='delete')
]
