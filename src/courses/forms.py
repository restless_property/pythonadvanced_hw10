"""
THe module contains code that renders data
into HTML code for the app. 'courses'.
"""

from courses.models import Course

from django.forms import ModelForm

import django_filters


class CourseFilter(django_filters.FilterSet):
    """
    The class processes search and filtering operations.
    """

    class Meta:
        model = Course
        fields = {
            'course_name': ["exact", "icontains"],
            'group': ["exact"],
            "teachers": ["exact"]
        }


class CourseBaseForm(ModelForm):
    """
    A form that converts "python"-related data types of instance attributes
    into relevant HTML representation.
    """

    class Meta:
        model = Course
        fields = "__all__"
        # fields = ["group_name", "subject_name", "lecturer", "mentor",
        #           "number_of_students", "average_score"]


class CourseCreateForm(CourseBaseForm):
    pass


class CourseUpdateForm(CourseBaseForm):
    """
    The class operates the edit of the data about a course.
    """

    class Meta(CourseBaseForm.Meta):
        model = Course
        fields = '__all__'
