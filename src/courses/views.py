"""
The module contains view-functions for the app. 'courses'.
"""

from courses.forms import CourseCreateForm, CourseFilter, CourseUpdateForm
from courses.models import Course

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView


# Create your views here.


def get_courses_with_filter(request):
    """
    ...
    """
    courses = Course.objects.all().select_related("group").prefetch_related("teachers")
    filt = CourseFilter(
        data=request.GET,
        queryset=courses)
    return render(request=request,
                  template_name='courses/courses_get_filter.html',
                  context={'filter': filt,
                           'courses': filt.qs})


def create_course(request):
    """
    ...
    """
    if request.method == 'POST':
        form = CourseCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('courses:filter'))
    elif request.method == 'GET':
        form = CourseCreateForm()
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request, 'courses/courses_create.html', {"fields": form})


def update_course(request, pk):
    """
    ...
    """
    course = get_object_or_404(Course, id=pk)
    if request.method == 'POST':
        form = CourseUpdateForm(data=request.POST, instance=course)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('courses:filter'))
    elif request.method == 'GET':
        form = CourseUpdateForm(instance=course)
    else:
        raise NotImplementedError('Only "GET"/"POST" methods are supported recently.')
    return render(request=request,
                  template_name='courses/courses_update.html',
                  context={'form': form,
                           'id': pk,
                           'course': course})


def delete_course(request, pk):
    """
    ...
    """
    course = get_object_or_404(Course, id=pk)
    if request.method == 'POST':
        course.delete()
        return HttpResponseRedirect(reverse('courses:filter'))
    return render(request=request,
                  template_name='courses/courses_delete.html',
                  context={'course': course})


class CourseFilterView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'courses/courses_get_filter.html'
    login_url = reverse_lazy('accounts:login')

    def get_filter(self):
        return CourseFilter(
               data=self.request.GET,
               queryset=self.model.objects.all()
               )

    # def get_queryset(self):
    #     return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['courses'] = self.get_filter().qs.select_related("group").prefetch_related("teachers")
        return context


class CourseCreateView(LoginRequiredMixin, CreateView):
    model = Course
    form_class = CourseCreateForm
    success_url = reverse_lazy('courses:filter')
    template_name = 'courses/courses_create.html'
    login_url = reverse_lazy('accounts:login')


class CourseUpdateView(LoginRequiredMixin, UpdateView):

    model = Course
    form_class = CourseUpdateForm
    success_url = reverse_lazy('courses:filter')
    template_name = 'courses/courses_update.html'
    login_url = reverse_lazy('accounts:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.get_object().id
        return context


class CourseDeleteView(LoginRequiredMixin, DeleteView):

    model = Course
    success_url = reverse_lazy('courses:filter')
    template_name = 'courses/courses_delete.html'
    login_url = reverse_lazy('accounts:login')
