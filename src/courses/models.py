"""
Models of the app 'courses'.
"""
import random

from courses.model_tools import make_course

from django.db import models

# Create your models here.


class Course(models.Model):
    """
    The class represents a subject studied
    during the educational process.

    Group fields:
    course_name: (charfield) the name of the studied subject
    group: (foreignkey: 1to1) the group which studies the course
    teachers: (foreignkey: M2M) the lecturers who teach that subjects
    """

    class Difficulty(models.IntegerChoices):
        TRIVIAL = 0, 'Trivial'
        EASY = 1, 'Easy'
        MEDIUM = 2, 'Medium'
        EXPERT = 3, 'Expert'
        HARDCORE = 4, 'Hardcore'

    course_name = models.CharField(max_length=64, null=False, blank=False)
    group = models.OneToOneField(
        to='groups.Group',
        on_delete=models.SET_NULL,
        null=True,
        related_name='subject'
        )
    teachers = models.ManyToManyField(
        to='teachers.Teacher',
        null=True,
        related_name='courses'
        )
    difficulty = models.PositiveSmallIntegerField(
        choices=Difficulty.choices,
        default=Difficulty.EASY
    )

    def __str__(self):
        return f'#ID {self.id}: {self.course_name}'

    def get_info(self):
        output = f'Course: {self.course_name}'
        try:
            if self.group is not None:
                output += f' = Group: {self.group.group_name}'
        except AttributeError:
            pass

        try:
            output += f' = Teachers: {self.teachers}'
        except AttributeError:
            pass
        return output

    @classmethod
    def generate_courses(cls, count: int):
        if count <= 0:
            raise ValueError("Cannot add a negative or zero number of records into the DB.")
        for _ in range(count):
            obj = cls(
                      course_name=make_course(),
                      group=None,
                      difficulty=random.choice(cls.Difficulty.values)
                      )
            obj.save()
